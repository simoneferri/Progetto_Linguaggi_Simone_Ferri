abstract class Expr {

  public abstract int calcola();

}


class PiuExpr extends Expr {
  private Expr sx, dx;

  public PiuExpr(Expr sx, Expr dx) {
    this.sx = sx;
    this.dx = dx;
  }

  public int calcola() {
    return sx.calcola() + dx.calcola();
  }

 
}


class MenoExpr extends Expr {
  private Expr sx, dx;

  public MenoExpr(Expr sx, Expr dx) {
    this.sx = sx;
    this.dx = dx;
  }

  public int calcola() {
    return sx.calcola() - dx.calcola();
  }

}


class PerExpr extends Expr {
  private Expr sx, dx;

  public PerExpr(Expr sx, Expr dx) {
    this.sx = sx;
    this.dx = dx;
  }

  public int calcola() {
    return sx.calcola() * dx.calcola();
  }

}


class DivisoExpr extends Expr {
  private Expr sx, dx;

  public DivisoExpr(Expr sx, Expr dx) {
    this.sx = sx;
    this.dx = dx;
  }

  public int calcola() {
    return sx.calcola() / dx.calcola();
  }
}


class UnMenoExpr extends Expr {
  private Expr e;

  public UnMenoExpr(Expr e) {
    this.e = e;
  }

  public int calcola() {
    return -e.calcola();
  }
}


class UnPiuExpr extends Expr {
  private Expr e;

  public UnPiuExpr(Expr e) {
    this.e = e;
  }

  public int calcola() {
    return e.calcola();
  }
}


class NumExpr extends Expr {
  private Integer num;

  public NumExpr(Integer num) {
    this.num = num;
  }

  public int calcola() {
    return num.intValue();
  }
}


class IdExpr extends Expr {
  private Descrittore descrittore;

  public IdExpr(Descrittore d) {
    descrittore = d;
  }

  public int calcola() {
    return descrittore.getValore();
  }
}

class ModuloExpr extends Expr{
  private Expr sx, dx;

  public ModuloExpr(Expr sx, Expr dx) {
    this.sx = sx;
    this.dx = dx;
  }

  public int calcola() {
    return sx.calcola() % dx.calcola();
  }
}

class AssegExpr extends Expr{
	private Expr val;
	private Descrittore desc;
	
	public AssegExpr(Descrittore desc, Expr val){
		this.desc = desc;
		this.val = val;
	}
	
	public int calcola(){
		desc.modificaValore(val.calcola());
		return desc.getValore();
	}
}

class IncrPostExpr extends Expr{

	private Descrittore desc;
	
	public IncrPostExpr(Descrittore d){
		desc = d;
	}
	
	public int calcola(){

		int tmp = desc.getValore();
		desc.modificaValore(tmp + 1);
		return tmp;
	}
}

class DecrPostExpr extends Expr{

	private Descrittore desc;
	
	public DecrPostExpr(Descrittore d){
		desc = d;
	}
	
	public int calcola(){
		
		int tmp = desc.getValore();
		desc.modificaValore(tmp - 1);
		return tmp;
	}
}

class IncrPreExpr extends Expr{

	private Descrittore desc;
	
	public IncrPreExpr(Descrittore d){
		desc = d;
	}
	
	public int calcola(){
		int tmp = desc.getValore() + 1;
		desc.modificaValore(tmp);
		return tmp;
	}
}

class DecrPreExpr extends Expr{

	private Descrittore desc;
	
	public DecrPreExpr(Descrittore d){
		desc = d;
	}
	
	public int calcola(){
		int tmp = desc.getValore() - 1;
		desc.modificaValore(tmp);
		return tmp;
	}
}


