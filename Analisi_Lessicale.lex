import java_cup.runtime.*;

%% 

%cup
%class Scanner


%{  //codice per associare la Symbol Factory
    ComplexSymbolFactory sf;
    public Scanner(java.io.Reader in, ComplexSymbolFactory sf) {
	this(in);
	this.sf = sf;
    }
%}

%{
public int currentLineNumber() {
return yyline + 1;
}
%}
%line


CIFRA = [:digit:]
LETTERA = [:letter:]
FINERIGA = \r | \n | \r\n
SPAZIATURA = [ \t\f] | {FINERIGA}	

%% 

"/"      	{return sf.newSymbol("DIVISO", ParserSym.DIVISO);}
"-"	    	{return sf.newSymbol("MENO",   ParserSym.MENO);}
"*" 	    {return sf.newSymbol("PER",    ParserSym.PER);}
"+"      	{return sf.newSymbol("PIU",    ParserSym.PIU);}
"%"      	{return sf.newSymbol("MODULO", ParserSym.MODULO);}
"("      	{return sf.newSymbol("TONDA_APERTA", ParserSym.TONDA_APERTA);}
")"      	{return sf.newSymbol("TONDA_CHIUSA", ParserSym.TONDA_CHIUSA);}
"="      	{return sf.newSymbol("ASSEG", ParserSym.ASSEG);}
";"      	{return sf.newSymbol("PUNTOVIR", ParserSym.PUNTOVIR);}
","      	{return sf.newSymbol("VIRGOLA", ParserSym.VIRGOLA);}
"++"     	{return sf.newSymbol("INCR", ParserSym.INCR);}
"--"      	{return sf.newSymbol("DECR", ParserSym.DECR);}
"while"		{return sf.newSymbol("WHILE", ParserSym.WHILE);}	
"keeping"	{return sf.newSymbol("KEEPING", ParserSym.KEEPING);}


{CIFRA}+ {return sf.newSymbol("NUMERO", ParserSym.NUMERO, new Integer(yytext()));}
{LETTERA}({LETTERA}|{CIFRA})* {return sf.newSymbol("IDENT", ParserSym.IDENT, yytext());}


{SPAZIATURA} { }

.        {return sf.newSymbol("err", ParserSym.error);}

<<EOF>>  {return sf.newSymbol("EOF", ParserSym.EOF);}

