import java_cup.runtime.*;
import java.io.*;
import java.util.Vector;

class FromFile {
  
  public static void main(String[] args) throws java.io.IOException {

    // creazione del canale di input
    BufferedReader in = null;
	String s = "";

	try {
		
		//Check errore di insertimento file o file non esistente
		try{
			in = new BufferedReader(new FileReader(args[0]));
		}catch (ArrayIndexOutOfBoundsException e){
			System.out.println("File non inserito");
			System.exit(1);
		}
		catch (IOException e){
			System.out.println("File non presente in questa directory");
			System.exit(1);
		}
		
		System.out.println("Nuovo File inserito correttamente");
		//creazione della symbol factory
		ComplexSymbolFactory sf = new ComplexSymbolFactory();

		//creazione dell'analizzatore lessicale
		Scanner scanner = new Scanner(in, sf);

		//creazione del parser
		Parser p = new Parser(scanner, sf);

		Symbol ris = p.parse();
		ExprConTab risultato = (ExprConTab) ris.value;
		  
		Vector<Expr> vettoreExpr = risultato.getVectorExpr();
		
		//valutazione dell'albero: notazione postfissa

		SymbolTable tabella = risultato.getSymbolTable();
	
		int risCond = 0;
			
		//creazione vettore di variabili della parte keeping
	
		Vector<String> vettoreKeep = new Vector<String>();
			
		//Se l'espressione possiede il while, salvo le
		//variabili di keeping in vettoreKeep	
		if(risultato.getWak() != null)
			vettoreKeep = risultato.getWak().getVectorKeep();  
			
		boolean flag = true;
		boolean keep = true;
		
		//Creazione canale per le espressioni contenute nel file specificato
		BufferedReader values = new BufferedReader(new InputStreamReader(System.in));


		do{ //Questo è per il WHILE
				
			/*
			 * richiede il valore delle variabili per la prima esecuzione.
			 * successivamente chiede solo le variabili che non sono nel
			 * keeping e lo imposta nella symbolTable.
			 * Inoltra controlla se l'utente ha inserito un numero 
			 * oppure altro fornendo un eventuale messaggio di errore.
			*/		  	
		
			for (Descrittore d : tabella) {
					
				if(flag || !vettoreKeep.contains(d.getIdentificatore())) {
				
					System.out.print("valore di " + d.getIdentificatore() + "? ");
				
					try{
						d.modificaValore(Integer.parseInt(values.readLine()));
					} catch (NumberFormatException e){
						System.out.println("Inserire un numero valido");  
					}
				}
			}
					
			flag = false;	
			
			//calcolo del risultato e controlla se vi è una AritmeticException
			//nel caso di divisione per 0 ad esempio.
			for(Expr a : vettoreExpr){
				try {
					System.out.println(a.calcola());
				} catch (ArithmeticException e) {
					System.err.println("Err");
				}  
			}
				
		/*
		 * Se l'espressione contiene un while allora controlla la
		 * condizione del while stesso, la valuta e assegna il
		 * risultato alla variabile risCond.
		*/			
		if(risultato.getWak() != null )
			risCond = risultato.getWak().getCond().calcola();
		else 
			keep = false;
		
		}while(risCond != 0);   // finchè il risultato del while è != 0 ripeti      
			 
    }catch (Exception e)  {
      System.out.println(e);
    }

  }
}


