import java_cup.runtime.*;
import java.io.*;

class TestToken {
  
  public static void main(String[] args) throws java.io.IOException {

    // creazione del canale di input
    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	boolean flag = true;

	do{
			//lettura della stringa da esaminare
			System.out.print("Inserire token: ");
			String s = in.readLine();
			//Se digito il token STOP, il Test si ferma.
			if(s.equals("STOP"))
				flag = false;
			//creazione della symbol factory
			ComplexSymbolFactory sf = new ComplexSymbolFactory();

			//creazione dell'analizzatore lessicale
			Scanner scanner = new Scanner(new StringReader(s), sf);
			
			Symbol symb;
			
			/* 
			 * Lo 0 è l'enum dell'EOF (Letto sulle API Online)
			*  con next_token prendo il simbolo successivo e con
			*  sym mi faccio restituire il tipo di simbolo rappresentato
			*  che è un numero int, guardando nell'API il sym associato 
			*  a EOF è lo 0. 
			*  Con value mi faccio restituire il valore del simbolo
			*  e con symb il suo tipo. 
			*/
			while(((symb = scanner.next_token()).sym) != 0)
				System.out.println("Token: " + symb.value + " ,Tipo Token:" + symb);
			
    } while(flag);

  }
}


