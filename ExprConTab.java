import java.util.Vector;
class ExprConTab {

  private SymbolTable tabella;
  private Vector<Expr> vettoreExpr;
  private WhAndKeep wak;


  public ExprConTab(SymbolTable s) {
    tabella = s;
	vettoreExpr = new Vector<Expr>();
  }

  public Vector<Expr> getVectorExpr(){
    return vettoreExpr;
  }

  public SymbolTable getSymbolTable() {
    return tabella;
  }
  
  public WhAndKeep getWak(){
	return wak;
  }
  
  public boolean addExpr(Expr e){
	return vettoreExpr.add(e);
  }
  
  public void setWak(Expr e){
	wak = new WhAndKeep(e);
  }
  

}

class WhAndKeep {

	private Expr cond;
	private Vector<String> vettoreKeep;
	
	public WhAndKeep(Expr c){
		cond = c;
		vettoreKeep = new Vector<String>();
	}
	
	public boolean addKeep(String k){
		return vettoreKeep.add(k);
	}
	
	public Vector<String> getVectorKeep(){
		return vettoreKeep;
	}
	
	public Expr getCond(){
		return cond;
	}
}


